import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Index from '@/components/Index'
import App from '@/App'
////////////////////////////////////////////////////////////student///////////////////////////////////////////////////////////////////

import Kundoluk_info from '@/components/Student/Kundoluk_info'
import info from '@/components/Student/info'
import SKundoluk_info from '@/components/Student/SKundoluk_info'
import HomeWork from '@/components/Student/HomeWork'
import Baa_katywuu from '@/components/Student/Baa_katywuu'
import raspisaniye from '@/components/Student/raspisaniye'
import WeekGradesAttendance from '@/components/Student/WeekGradesAttendance'
import HomeWorkWeek from '@/components/Student/HomeWorkWeek'
///////////////////////////////////////////////////////////////////Parenr///////////////////////////////////////////////////////////
import E_info from '@/components/parent/E_info'
import P_info from '@/components/parent/P_info'
import Hwork from '@/components/parent/Hwork'
import Baa from '@/components/parent/Baa'
import Praspisaniye from '@/components/parent/Praspisaniye'
import PE_info from '@/components/parent/PE_info'
import childrens_table from '@/components/parent/childrens_table'
////////////////////////////////////teacher//////////////////////////////////////////////////////////////////////////////////
import T_info from '@/components/teacher/T_info'
import Katyshuu from '@/components/teacher/Katyshuu'
import Klasstar from '@/components/teacher/Klasstar'
import T_raspisaniye from '@/components/teacher/T_raspisaniye'
import E_maalymat from '@/components/teacher/E_maalymat'
import Cheyrek from '@/components/teacher/Cheyrek'
import Jumalar from '@/components/teacher/Jumalar'
import Homcheyrek from '@/components/teacher/Homcheyrek'
import Homklasstar from '@/components/teacher/Homklasstar'
import Homjumalar from '@/components/teacher/Homjumalar'
import T_homework from '@/components/teacher/T_homework'
////////////////////////////////////////////////////supervisor//////////////////////////////////////////////////////////////
import S_info from '@/components/supervisor/S_info'
import SE_info from '@/components/supervisor/SE_info'
import Smenu from '@/components/supervisor/Smenu'
import Spisok from '@/components/supervisor/Spisok'
import Sraspisaniye from '@/components/supervisor/Sraspisaniye'
Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      component: Index
    },
    {
      path: '/app',
      component: App
    },
    {
      path: '/info',
      component: info,
      props:true,
    },
    {
      path: '/SKundoluk_info',
      component: SKundoluk_info
    },
    {
      path: '/Kundoluk_info',
      component: Kundoluk_info
    },
    {
      path: '/WeekGradesAttendance',
      component: WeekGradesAttendance
    },
    {
      path: '/HomeWorkWeek',
      component: HomeWorkWeek
    },
    {
      path: '/HomeWork',
      component: HomeWork
    },
    {
      path: '/Baa_katywuu',
      component: Baa_katywuu
    },
    {
      path: '/raspisaniye',
      component: raspisaniye
    },
    ///////////////////////
    {
      path: '/E_info',
      component: E_info
    },
    {
      path: '/PE_info',
      component: PE_info
    },
    {
      path: '/P_info',
      component: P_info
    },
    {
      path: '/Baa',
      component: Baa
    },
    {
      path: '/Praspisaniye',
      component: Praspisaniye
    },
    {
      path: '/Hwork',
      component: Hwork
    },
    {
      path: '/childrens_table',
      component: childrens_table
    },
    ///////////////////
    {
      path: '/E_maalymat',
      component: E_maalymat
    },
    {
      path: '/T_info',
      component: T_info
    },
    {
      path: '/Katyshuu',
      component: Katyshuu
    },
    {
      path: '/Klasstar',
      component: Klasstar
    },
    {
      path: '/Homklasstar',
      component: Homklasstar
    },
    {
      path: '/T_raspisaniye',
      component: T_raspisaniye
    },
    {
      path: '/Cheyrek',
      component: Cheyrek
    },
    {
      path: '/Jumalar',
      component: Jumalar
    },
    {
      path: '/Homjumalar',
      component: Homjumalar
    },
    {
      path: '/Homcheyrek',
      component: Homcheyrek
    },
    {
      path: '/T_homework',
      component: T_homework
    },
    //////////////////////////
  {
    path: '/S_info',
    component: S_info
  },

  {
    path: '/SE_info',
    component: SE_info
  },

  {
    path: '/Smenu',
    component: Smenu
  },

  {
    path: '/Sraspisaniye',
    component: Sraspisaniye
  },

  {
    path: '/Spisok',
    component: Spisok
  },
  {
    path: '/Exit',
    component: Index
  },
  ],
  mode:'history'//http://localhost:8080/#/home tegi # ti alyp koyot
})
