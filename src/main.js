// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import 'bootstrap' 
import router from './router/index'
import store from './store/store'
import VideoBg from 'vue-videobg'
import Vuex from 'vuex'
Vue.use(Vuex);
Vue.config.productionTip = false;
Vue.component('video-bg', VideoBg)

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store//  router:router

})




/*new Vue({
  el: '#index',
  router,
  components: { Index },
  template: '<Index/>'
})*/
