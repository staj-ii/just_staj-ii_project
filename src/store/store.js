import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios'

Vue.use(Vuex);
export default new  Vuex.Store({
    state: {
        post:"https://e-kundoluk-flask-server.herokuapp.com/api/login/",
        StudentInfo:[],
        StudentId:'',
        id_klass:'',
        Table:{},
        weeks:[],
        GradesAttendance:[],
        HomeWork:[],
      /************************************/
      ParentInfo:[],
      childrens:[],
      id_klass_parent_child:'',
      id_student_parent_child:'',
      /**************************************/
      id_parent:'',
      nchild:'',

      SupervisorInfo:[],
      TeacherInfo:[],

    },
   getters:{
    table: state => {return state.Table},
     weeks:state => {return state.weeks},
     GradesAttendance:state => {return state.GradesAttendance},
     HomeWork:state => {return state.HomeWork},

     id_parent:state => {return state.id_parent},
     childrens:state => {return state.childrens},
     Nchildrens:state => {return state.nchild},

  },
    mutations: {
      /**************************Student*****************************************/
        GetStudentInfo (state, payload) {
            state.StudentInfo = payload
            state.StudentId= state.StudentInfo.id_student
            state.id_klass= state.StudentInfo.id_klass
        },
      /***************************Parent****************************************/
        GetParentInfo (state, payload) {
            state.ParentInfo = payload
            console.log('id_parent:',payload.id_parent)
            state.id_parent = payload.id_parent
        },
      /**********************teacher*******************************************/
        GetTeacherInfo (state, payload) {
            state.TeacherInfo = payload
        },
      /**********************Supervisor*******************************************/
        GetSupervisorInfo (state, payload) {
            state.SupervisorInfo = payload
        },
        TimeTableMutation(state,TimeTable){
          state.Table=TimeTable;
          console.log('TimeTableMutation',state.Table)
        },

        Weeks(state,payload){
            state.weeks=payload
        },
      GradesAttendanceMut(state,payload){
          state.GradesAttendance=payload
      },
      HomeWorkMutation(state,payload){
          state.HomeWork=payload
      },
      /***********************************************parent************************************************************/
      Childrens(state,payload){
        state.childrens=payload.key1;
        state.nchild=payload.key2
      },
      id_klass_id_student_for_parent(state,payload){
        state.id_klass_parent_child=payload.key1
        state.id_student_parent_child=payload.key2
      }

    },
    actions: {
      TimeTableAction({commit},klass_id) {
        console.log('klass_id',klass_id);
        axios.get('https://e-kundoluk-flask-server.herokuapp.com/api/timetable/klass/'+klass_id) .then((response) => {
          let time_table = response.data;
          const lenth = response.data.length;
          console.log('time_table:',time_table);
          console.log(lenth);

          let temp = {}
          time_table.forEach((item)=>{
            if(temp["day"+item.day]=== undefined){
              temp["day"+item.day] = []
            }
            temp["day"+item.day].push(item)
          });
          console.log('temp:', temp)

          commit('TimeTableMutation', temp)
        })



      },
      WeekGradesAttendance({commit}) {
        let weeks =[]
        axios.get('https://e-kundoluk-flask-server.herokuapp.com/api/week').then(response => {
          weeks = response.data;
          console.log('Week:',weeks);
          commit('Weeks', weeks)
        })

      },
      WeekGradesAttendanceForParent({commit},data){
        commit('id_klass_id_student_for_parent',data)
        let weeks =[]
        axios.get('https://e-kundoluk-flask-server.herokuapp.com/api/week').then(response => {
          weeks = response.data;
          console.log('Week:',weeks);
          commit('Weeks', weeks)
        })
      },
      GradesAttendance({commit},data){
        let id_week=data.key1;
        let klassId=data.key2;
        let StudentId=data.key3;
        console.log('A:',data);
        axios.get('https://e-kundoluk-flask-server.herokuapp.com/api/gradesattendance/student/'+id_week+'/'+klassId+'/'+StudentId).then(response => {

          console.log('GradesAttendance:',response.data);
          commit('GradesAttendanceMut', response.data)
        })
      },
      HomeWorkAction({commit},data){
        let id_week=data.key1;
        let klassId=data.key2;
        console.log('A:',data);
        axios.get('https://e-kundoluk-flask-server.herokuapp.com/api/homework/student/'+id_week+'/'+klassId).then(response => {

          console.log('HomeWork:',response.data);
          commit('HomeWorkMutation', response.data)
        })

      },

      Childrens({commit},id_parent){
        console.log("Childrens id_parent :",id_parent)
        axios.get('https://e-kundoluk-flask-server.herokuapp.com/api/parent/childrens/'+id_parent) .then((response) => {

          console.log('Childrens:',response.data);
          let n =response.data.length;
          const data = {'key1':response.data,'key2': n}
           commit('Childrens', data)
        })

      },
    }
    //
    // actions: {
    //   initStore({commit}) {
    //     return this.$axios.$get('https://e-kundoluk-flask-server.herokuapp.com/api/timetable/klass/' + klass).then(data => {
    //       let tempObject = {}
    //       data.forEach(doc => {
    //         tempObject[doc._id] = doc
    //       });
    //       commit('SET_CATEGORIES', tempObject)
    //     }).catch(error => {
    //       console.log(error)
    //     })
    //   },
    //   TimeTable({commit},payload){
    //     let day1= [];
    //     let day2= [];
    //     let day3= [];
    //     let day4= [];
    //
    //
    //    payload.forEach((item)=>{
    //       if(item.day===1)
    //         {
    //           console.log(item.day)
    //           day1[item.day].push(item)
    //         }
    //      if(item.day===2)
    //      {
    //        console.log(item.day)
    //        day2[item.day].push(item)
    //      }
    //
    //
    //
    //
    //     });
    //     console.log(day1)
    //     console.log(day2)
    //   }
    //
    //   }

/*
export const state = () => ({
  list: {}
})

export const getters = {
  categories: state => state.list
}

export const mutations = {
  SET_CATEGORIES: (state, categories) => {
    state.list = categories
  }
}

export const actions = {
  initStore({commit}, payload) {
    return this.$axios.$get('/api/categories').then(data => {
      let tempObject = {}
      data.forEach(doc => {
        tempObject[doc._id] = doc
      });
      commit('SET_CATEGORIES', tempObject)
    }).catch(error => {
      console.log(error)
    })
  },
  update({commit, state , dispatch}, payload) {
    let form = new FormData();
    form.append('file' , payload.file);
    form.append('fileDetail' , payload.fileDetail);
    form.append('form', JSON.stringify(payload.form));
    return this.$axios.$put('/api/category', form).then((data) => {
      dispatch('initStore')
    })
  },
  del({commit, state}, payload) {
    console.log(payload)
    return this.$axios.$delete('/api/category/' + payload.id).then(() => {
      let tempObject = Object.assign({}, state.list)
      delete tempObject[payload.id]
      commit('SET_CATEGORIES', tempObject)
    })
  },
  add({commit, state}, payload) {
    let form = new FormData();
    form.append('file' , payload.file);
    form.append('fileDetail' , payload.fileDetail);
    form.append('form', JSON.stringify(payload.form));
    this.$axios.$post('/api/category/new', form).then(data => {
      let tempObject = Object.assign({}, state.list)
      tempObject[data._id] = data
      commit('SET_CATEGORIES', tempObject)
    })
  }
}
*/

})
